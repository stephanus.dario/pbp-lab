from django.shortcuts import render
from django.http import HttpResponseRedirect
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.contrib.auth.decorators import login_required
@login_required(login_url='/admin/login/?next=/lab-4')
@login_required(login_url='/admin/login/?next=/lab-4/add-note')
@login_required(login_url='/admin/login/?next=/lab-4/note-list')

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
    form = NoteForm(request.POST or None)
    if form.is_valid():
        form.save()
        if request.method == 'POST':
            return HttpResponseRedirect("/lab-4")
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)
