from django.urls import path
from .views import index, json, xml

urlpatterns = [
    path('', index, name='index'),
    path('xml/', xml, name = 'xml'),
    path('json/', json, name = 'json'),
    # TODO Add friends path using friend_list Views
]
