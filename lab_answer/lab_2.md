Nama  : Stephanus Dario
Kelas : PBP A
NPM   : 2006527550

1. Apakah perbedaan antara JSON dan XML?

json dan xml adalah salah satu dari berbagai format yang digunakan untuk melakukan pertukaran data dari web server. Dimana XML(Etensible Markup Language) adalah suatu format yang menggunakan sintaks berupa tag. Dimana XML akan menampilkan bahasa markup dari yang sudah kita tampilkan. pengertian dari bahasa markup ialah sebuah cara untuk menambahkan informasi tambahan dari teks yang sudah ditampilkan. sedangkan JSON hadir untuk mewakili objek objek yang ada di dalamnya. Perbedaan-perbedaan yang ada pada keduanya adalah:

-Json disimpan dalam map dengan key dan value. Sedangkan XML ada dalam tree structure.
-Json memiliki banyak tipe data, sedangkan XML hanya memiliki tipe data string.
-Json memiliki syntax yang lebih mudah dipahami sedangkan XML memiliki syntax yang lebih rumit.

2. Apakah perbedaan antara HTML dan XML?
Perbedaan yang ada adalah:
-XML merupakan format untuk melakukan transfer data, sedangkan HTML adalah format untuk menampilkan data
-Tag pada XML dibebaskan pada pengguna, sedangkan HTML memiliki tag yang terbatas.
-XML hadir setelah adanya konten, sedangkan HTML hadir setelah adanya format yang ditulis.