import 'package:flutter/material.dart';

void main() => runApp(MyApp());

/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
              backgroundColor: Colors.teal.shade50,
              title: Text('Symptomps And Medicine',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black))),
          backgroundColor: Colors.white,
          body: Container(
              child: ListView(children: <Widget>[
            buildCard(
                "Hidung Tersumbat\n",
                "Hidung tersumbat disebabkan oleh apa pun yang menyebabkan terjadinya iritasi atau peradangan pada jaringan hidung. Ketika hidung mengalami iritasi, sistem saraf akan terangsang, sehingga katup pembuluh darah terbuka. Akibatnya, darah yang mengalir ke dalam hidung pun lebih banyak.\n",
                "Obat: Dekongestan, Antihistamin, Cairan saline, Oxymetazoline"),
            buildCard(
                "Gangguan Pencernaan\n",
                "Penyakit refluks asam lambung atau gastroesophageal reflux disease (GERD) adalah kondisi ketika asam lambung naik ke esofagus (kerongkongan). Gangguan Pencernaan juga bisa disebabkan oleh pola makan yang tidak baik, serta kandungan makanan yang tidak baik bagi kesehatan tubuh.\n",
                "Obat: Antasida, Paracetamol, Probiotik")
          ]))),
    );
  }
}

Card buildCard(String nama, String penjelasan, String obat) {
  return Card(
      child: Container(
    width: 250,
    height: 270,
    padding: new EdgeInsets.all(10.0),
    child: Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      color: Colors.indigo.shade50,
      elevation: 20,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(nama),
          Text(penjelasan),
          Text(obat),
          ButtonBar(
            children: <Widget>[
              RaisedButton(
                child: const Text('Edit'),
                onPressed: () {/* ... */},
                color: Colors.green,
              ),
              RaisedButton(
                child: const Text('Delete'),
                onPressed: () {/* ... */},
                color: Colors.red,
              ),
            ],
          ),
        ],
      ),
    ),
  ));
}
